<?php
/**
 * @package Redelocker Boton
 */
/*
* Plugin Name: Redelocker Boton
* Plugin URI:
* Description: Boton de mapa Redelocker
* Version: 1.0.0
* Author: Redelocker, antojqp
* Author URI: redelocker.com
* License: GPLv2 or later
* Requires at least: 5.2
* Requires PHP:      7.2
* Text Domain: redelocker-boton
*/ 


if ( ! defined( 'WPINC' ) ) {
	die('also no');
}

if (! defined('ABSPATH')) {
	die('no');
}
if (! function_exists('add_action')) {
	die('no, check your wordpress');
}
if ( file_exists(dirname(__FILE__).'/vendor/autoload.php') ) {
	require_once dirname(__FILE__).'/vendor/autoload.php';
}
/**
 * Define constants
 */
//TODO:: CHANGE TO RBW
define("REDE_PREFIX", "rede");
define("REDE_VERSION", 1);
/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	Inc\Init::register_services();
	add_action('woocommerce_shipping_init', 'redelocker_shipping_method');
	function redelocker_shipping_method() {
	
		if ( ! class_exists( 'WC_Redelocker_Shipping_Method' ) ) {
			class WC_Redelocker_Shipping_Method extends WC_Shipping_Method {
	
				public function __construct( $instance_id = 0) {
					$this->id = 'redelocker_shipping_method';
					$this->instance_id = absint( $instance_id );
					$this->domain = REDE_PREFIX;
					$this->method_title = __( 'Enviar a Redelocker', $this->domain );
					$this->method_description = __( 'Usar este metodo para Enviar con redelocker', $this->domain );
					$this->supports = array(
						'shipping-zones',
						'instance-settings',
						'instance-settings-modal',
					);
					$this->init();
				}
				## Load the settings API
				function init() {
					$this->init_form_fields();
					$this->init_settings();
					$this->enabled = $this->get_option( 'enabled', $this->domain );
					$this->title   = $this->get_option( 'title', $this->domain );
					$this->info    = $this->get_option( 'info', $this->domain );
					add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
				 }
				function init_form_fields() {
					
					$this->instance_form_fields = array(
						'title' => array(
							'type'          => 'text',
							'title'         => __('Title', $this->domain),
							'description'   => __( 'Envia con Redelocker.', $this->domain ),
							'default'       => __( 'Enviar con Redelocker ', $this->domain ),
						),
						'cost' => array(
							'type'          => 'number',
							'title'         => __('Costo', $this->domain),
							'description'   => __( 'Especifica un costo por el envio por orden', $this->domain ),
							'default'       => '',
						),
					);
					
				
				}
	
				public function calculate_shipping( $packages = array() ) {
					$this->init_settings();
					$rate = array(
						'id'       => $this->id,
						'label'    => $this->title,
						'cost'     => $this->get_option( 'cost' ),
						'calc_tax' => 'per_order'
					);
					$this->add_rate( $rate );
				}
			}
		}
	}
	
	add_filter('woocommerce_shipping_methods', 'add_redelocker_shipping');
	function add_redelocker_shipping( $methods ) {
		$methods['redelocker_shipping_method'] = 'WC_Redelocker_Shipping_Method';
		return $methods;
	}
}
/**
 * The code that runs during plugin activation
 */
function activate_rede_plugin() {
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_rede_plugin' );
