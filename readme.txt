=== Redelocker Boton WooCommerce ===
Contributors: antojqp
Tags: woocommerce, ecommerce
Requires PHP: 7.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A plugin from redelocker in order to add a button and a shipping method from ourselves

== Installation ==
The user will need woocommerce in order to use the plugin