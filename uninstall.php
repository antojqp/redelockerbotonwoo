<?php
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
if (! defined('REDE_PREFIX')) {
	define( "REDE_PREFIX", "rede" );
}
delete_option( REDE_PREFIX.'_map_active', '0');
delete_option( REDE_PREFIX.'_user', 'public');
delete_option( REDE_PREFIX.'_map_key', '');
delete_option( REDE_PREFIX.'_products_table', '');
delete_option( REDE_PREFIX.'_map_active', '0');
delete_option( REDE_PREFIX.'_auto_open', '0');
delete_option( REDE_PREFIX.'_precio_redelocker', '1');
