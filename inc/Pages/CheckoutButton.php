<?php
/**
 * @package Redelocker Boton
 */
namespace Inc\Pages;
use Inc\Base\BaseController;


class CheckoutButton extends BaseController
{

	public function register()
	{
		add_action( 'woocommerce_checkout_order_review', array( $this, 'add_redelocker_checkout' ), 10 );
		add_action( 'woocommerce_review_order_before_shipping', array($this,'redelocker_js_check') );
		add_action( 'woocommerce_checkout_process', array($this,'redelocker_field_process'));
		add_action( 'woocommerce_checkout_update_order_meta', array($this,'redelocker_checkout_field_update_order_meta') );
		add_action( 'woocommerce_admin_order_data_after_billing_address', array($this,'redelocker_checkout_field_display_admin_order_meta'), 10, 1 );
		// add_action( 'woocommerce_order_details_after_order_table', );
	}

	/**
	 * add the redelocker button next to the add to cart default button of wordpress
	 */
	public function add_redelocker_checkout() {
		$user    = get_option( $this->plugin_db_prefix.'_user' );
		$map     = get_option( $this->plugin_db_prefix.'_map_active' );
		$map_key = get_option( $this->plugin_db_prefix.'_map_key' );
		$auto_open = get_option( $this->plugin_db_prefix.'_auto_open' );
		$cedula 	= get_option( $this->plugin_db_prefix.'_cedula' );
		$celular 	= get_option( $this->plugin_db_prefix.'_celular' );
		$email 		= get_option( $this->plugin_db_prefix.'_email' );

		?>
		<div id="redelocker" style = "display: none">

			<button id='redebutton' style="background:red; color:white;" >Recibe con RedeLocker</button>
			<input type="hidden" id="rede_input" name="rede_input" placeholder="<?php _e( 'Redelocker' ); ?>">
			<div id='rededata' redeusr=''></div>
			<div id="camposExtra" class="form-row"></div>
		</div>
			<script>
				var myRedelocker = new redeData({
					map: <?php echo $map;?>,
					buttonId: 'redebutton',
					inputId: 'rede_input',
					usr: '<?php echo $user?>',
					autoOpen: 0<?php echo $auto_open?>,
				});
				let input = document.getElementById('rede_input');
				let button = document.getElementById('redebutton');
				input.addEventListener("change", function () {
					button.innerHTML = `Envio a: Redelocker ${input.value}`;
					if ("createEvent" in document) {
						var evt = document.createEvent("HTMLEvents");
						evt.initEvent("change", false, true);
						button.dispatchEvent(evt);
					}else button.fireEvent("onchange");
				})
				<?php
				if ($cedula) {
				?>
				button.addEventListener("change", function () {
					let oldInput = document.getElementById("rede_cedula")
					if (document.body.contains(oldInput)) {
						return;
					}
					let container = document.getElementById('camposExtra');
					let node = document.createElement("INPUT");                 // Create a <li> node
					let label = document.createElement("LABEL");
					let labelText = document.createTextNode("Cedula:*");
					label.appendChild(labelText);
					label.setAttribute("for", "rede_cedula")
					node.setAttribute("type", "number")
					node.setAttribute('id',"rede_cedula")
					node.setAttribute('name', "rede_cedula")
					function verifCedula(ci){
						const vector = [8,1,2,3,4,7,6];
						let ced=ci.split("").map((d)=>parseInt(d));
						let dv = vector.reduce(
									(prev,curr,i)=>
										prev+curr*ced[i],
										1) % 10 - 1;
						return dv == ci[ci.length - 1]
					}
					container.appendChild(label);
					container.appendChild(node);
					let newInput = document.getElementById("rede_cedula");
					newInput.addEventListener("blur", function () {
						if (this.value.length != 8) {
							return
						}
						let valida = verifCedula(this.value)
						if (!valida) {
							this.className += " woocommerce-invalid is-invalid"
							alert("Esta cedula no cumple con los estandares de Uruguay si esta introduciendo otro tipo de documento no le preste atencion a este mensaje")
						}
					})

				})
				<?php
				}
				?>
				<?php
				if ($celular) {
				?>
				button.addEventListener("change", function () {
					let oldInput = document.getElementById("rede_celular")
					if (document.body.contains(oldInput)) {
						return;
					}
					let container = document.getElementById('camposExtra');
					let node = document.createElement("INPUT");                 // Create a <li> node
					let label = document.createElement("LABEL");
					let labelText = document.createTextNode("Celular:*");
					label.appendChild(labelText);
					label.setAttribute("for", "rede_celular")
					node.setAttribute("type", "number")
					node.setAttribute('id',"rede_celular")
					node.setAttribute('name', "rede_celular")
					container.appendChild(label);
					container.appendChild(node);
				})
				<?php
				}
				?>
								<?php
				if ($email) {
				?>
				button.addEventListener("change", function () {
					let oldInput = document.getElementById("rede_email")
					if (document.body.contains(oldInput)) {
						return;
					}
					let container = document.getElementById('camposExtra');
					let node = document.createElement("INPUT");                 // Create a <li> node
					let label = document.createElement("LABEL");
					let labelText = document.createTextNode("Email:* ");
					label.appendChild(labelText);
					label.setAttribute("for", "rede_email")
					node.setAttribute("type", "email")
					node.setAttribute('id',"rede_email")
					node.setAttribute('name', "rede_email")
					container.appendChild(label);
					container.appendChild(node);
				})
				<?php
				}
				?>
			</script>
		<?php
		if($map){
			?>
			<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $map_key;?>"></script>
			<?php
		}
	}
	/**
	 * Add the js to check whether the option is selected
	 */
	public function redelocker_js_check()
	{
		?>
		<script type="text/javascript">
			(function($){
				$( 'form.checkout' ).on( 'change', 'input[name^="shipping_method"]', function() { 
					if ($(this).val() == "redelocker_shipping_method") {
						$( "#redelocker" ).show();
					}else{
						$( "#redelocker" ).hide();
					}

				});
				// if they already have selected the redelocker shipping method
				let selectedDefault = $( 'form.checkout' ).find('input.shipping_method:checked');
				if (selectedDefault.val() == "redelocker_shipping_method") {
					$( "#redelocker" ).show();
					
				}else{
					$( "#redelocker" ).hide();
				}
				// if they only have redelocker as the only shipping method

				let shippingDefault = $( 'form.checkout' ).find('input[name^="shipping_method"]');
				if(shippingDefault.length == 1 && shippingDefault.val() == "redelocker_shipping_method"){
					$( "#redelocker" ).show()
				}
				
			})(jQuery);
		</script>
		<?php
	}
	/**
	 * Process the shipping method if the method is redelocker and a locker has not been selected the it returns 
	 * a response back
	 * @return wc_notice
	 */
	public function redelocker_field_process()
	{
		if( $_POST['shipping_method'][0] == "redelocker_shipping_method" && empty($_POST["rede_input"]) ){
			wc_add_notice( __( 'Por favor seleccione un locker ' ), 'error' );
		}
		if (isset($_POST['rede_cedula']) && empty($_POST['rede_cedula'])) {
			wc_add_notice( __( 'Por favor completa el campo de cedula ' ), 'error' );
		}
		if (isset($_POST['rede_celular']) && empty($_POST['rede_cedula'])) {
			wc_add_notice( __( 'Por favor completa el campo de celular ' ), 'error' );
		}
		if (isset($_POST['rede_email']) && empty($_POST['rede_cedula'])) {
			wc_add_notice( __( 'Por favor completa el campo de email ' ), 'error' );
		}
	}
	/**
	 * Adds the meta field to the checkout data
	 */
	public function redelocker_checkout_field_update_order_meta( $order_id )
	{
		if ( ! empty( $_POST['rede_input'] ) && $_POST['shipping_method'][0] == "redelocker_shipping_method" ) {
			update_post_meta( $order_id, 'Redelocker', sanitize_text_field( $_POST['rede_input'] ) );
			if (isset($_POST['rede_email'])) {
				update_post_meta( $order_id, 'Redelocker email', sanitize_text_field( $_POST['rede_email'] ) );
			}
			if (isset($_POST['rede_cedula'])) {
				update_post_meta( $order_id, 'Redelocker cedula', sanitize_text_field( $_POST['rede_cedula'] ) );
			}
			if (isset($_POST['rede_celular'])) {
				update_post_meta( $order_id, 'Redelocker celular', sanitize_text_field( $_POST['rede_celular'] ) );
			}
		}
	}
	/**
	 * Display field value on the order edit page
	 */
	public function redelocker_checkout_field_display_admin_order_meta($order)
	{
		echo '<p><strong>'.__( 'Redelocker' ).':</strong> ' . get_post_meta( $order->get_id(), 'Redelocker', true ) . '</p>';
		$cedula 	= get_option( $this->plugin_db_prefix.'_cedula' );
		$celular 	= get_option( $this->plugin_db_prefix.'_celular' );
		$email 		= get_option( $this->plugin_db_prefix.'_email' );
		echo '<p><strong>'.__( 'Redelocker celular' ).':</strong> ' . get_post_meta( $order->get_id(), 'Redelocker celular', true ) . '</p>';
		echo '<p><strong>'.__( 'Redelocker cedula' ).':</strong> ' . get_post_meta( $order->get_id(), 'Redelocker cedula', true ) . '</p>';
		echo '<p><strong>'.__( 'Redelocker email' ).':</strong> ' . get_post_meta( $order->get_id(), 'Redelocker email', true ) . '</p>';
	}

}
