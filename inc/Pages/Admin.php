<?php
/**
 * @package Redelocker Boton
 */
namespace Inc\Pages;
use Inc\Base\BaseController;
use Inc\Api\SettingsApi;

class Admin extends BaseController
{

    public $settings;
    public $pages = [];



    public function register()
    {
		add_action( 'admin_menu', array($this,'rede_add_admin_menu') );
		add_action( 'admin_init', array($this,'rede_settings_init') );
	}
	



	public function rede_add_admin_menu() { 

		add_options_page('Redelocker Plugin', 'Redelocker', 'manage_options', 'redelocker_plugin', array($this,'rede_options_page'));
		add_menu_page( 'Redelocker Plugin', 'Redelocker', 'manage_options', 'redelocker_plugin', array($this,'rede_options_page') );

	}


	public function rede_settings_init() { 

		//registrar seccion
		add_settings_section('rede_settings_section', __( 'Configuracion del plugin', 'redelocker-boton-plugin' ), array($this,'rede_settings_section_callback'), 'rede_settings');
		//usuario de redelocker
		register_setting( 'rede_settings', 'rede_user' );
		add_settings_field( 'user', __( 'Usuario de Redelocker', 'redelocker-boton-plugin' ), array($this,'add_user_setting'), 'rede_settings', 'rede_settings_section' );
		//inicializar con mapa
		register_setting( 'rede_settings', 'rede_map_active' );
		add_settings_field( 'map', __( 'Inicializar el boton con mapa', 'redelocker-boton-plugin' ), array($this,'add_map_active_setting'), 'rede_settings', 'rede_settings_section' );
		//apikey del mapa
		register_setting( 'rede_settings', 'rede_map_key' );
		add_settings_field( 'map-key', __( 'Api Key usada para generar el contenedor de Google Maps', 'redelocker-boton-plugin' ), array($this,'add_map_key_setting'), 'rede_settings', 'rede_settings_section' );
		//autoOpen
		register_setting( 'rede_settings', 'rede_auto_open' );
		add_settings_field( 'auto-open', __( 'Activacion automatica del contenedor de ubicacion', 'redelocker-boton-plugin' ), array($this,'add_auto_open_setting'), 'rede_settings', 'rede_settings_section' );
		//inputs opcionales
		register_setting( 'rede_settings', 'rede_celular' );
		register_setting( 'rede_settings', 'rede_email' );
		register_setting( 'rede_settings', 'rede_cedula' );
		add_settings_field( 'auto-open', __( 'Campos necesarios para crear tus reservas en redelocker', 'redelocker-boton-plugin' ), array($this,'add_optional_fields_settings'), 'rede_settings', 'rede_settings_section' );
	}
	/**
	 * defines de section 
	 */
	public function rede_settings_section_callback()
	{
		echo "Aca puedes cambiar la configuracion del plugin de redelocker";
	}

	public function add_map_active_setting()
	{
		$setting = get_option('rede_map_active');
		?>
		<select name="rede_map_active">
		<option value="0"<?php if(!$setting){echo "selected";}?>>Inactivo</option>
		<option value="1"<?php if($setting){echo "selected";}?>>Activo</option>
		</select>
		<?php
	}

	public function add_user_setting()
	{
		$setting = get_option( 'rede_user' );
		?>
		<input type="text" name="rede_user" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
		<?php
	}

	public function add_map_key_setting()
	{
		$setting = get_option( 'rede_map_key' );
		?>
		<input type="text" name="rede_map_key" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
		<?php
	}
	public function add_auto_open_setting()
	{
		$setting = get_option( 'rede_auto_open' );
		?>
		<select name="rede_auto_open">
		<option value="0"<?php if( ! $setting ){echo "selected";}?>>Inactivo</option>
		<option value="1"<?php if( $setting ){echo "selected";}?>>Activo</option>
		</select>
		<?php
	}
	public function add_optional_fields_settings()
	{
		$cedula = get_option( 'rede_cedula' );
		$email = get_option( 'rede_email' );
		$celular = get_option( 'rede_celular' );
		
		?>
		<label for="celular">Celular:</label>
		<select id="celular" name="rede_celular">
		<option value="0"<?php if( ! $celular ){echo "selected";}?>>Inactivo</option>
		<option value="1"<?php if( $celular ){echo "selected";}?>>Activo</option>
		</select>
		<br>
		<label for="cedula">Cedula:</label>
		<select id='cedula' name="rede_cedula">
		<option value="0"<?php if( ! $cedula ){echo "selected";}?>>Inactivo</option>
		<option value="1"<?php if( $cedula ){echo "selected";}?>>Activo</option>
		</select>
		<br>
		<label for="email">Correo Electronico:</label>
		<select id='email' name="rede_email">
		<option value="0"<?php if( ! $email ){echo "selected";}?>>Inactivo</option>
		<option value="1"<?php if( $email ){echo "selected";}?>>Activo</option>
		</select>
		
		<?php
	}

	public function rede_options_page() { 

			?>
			<form action='options.php' method='post'>

				<h2> Redelocker Boton WooCommerce</h2>

				<?php
				settings_fields( 'rede_settings' );
				do_settings_sections( 'rede_settings' );
				submit_button();
				?>

			</form>
			<?php

	}
}
