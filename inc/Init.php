<?php
/**
 * @package Redelocker Boton
 */

namespace Inc;

final class Init
{
    /**
     * 
     * @method get_services
     * @return array services to initialize
     */
    public static function get_services()
    {
        return [
            Pages\Admin::class,
            Pages\CheckoutButton::class,
            Base\Enqueue::class,
        ];
    }

    /**
     * Executes the register function on each service from get_services
     * @return void
     */
    public static function register_services()
    {
        foreach(self::get_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        };
    }
    /**
     * @param class $class 
     * @return class instance 
     */
    private static function instantiate($class)
    {
        $service = new $class;

        return $service;
    }
}
