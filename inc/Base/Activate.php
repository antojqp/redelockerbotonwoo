<?php
/**
 * @package Redelocker Boton
 */
 namespace Inc\Base;

 class Activate
 {
	 public static function activate()
	 {
		flush_rewrite_rules();
		add_option( REDE_PREFIX.'_map_active', '0');
		add_option( REDE_PREFIX.'_user', 'public');
		add_option( REDE_PREFIX.'_map_key', '');
		add_option( REDE_PREFIX.'_products_table', '');
		add_option( REDE_PREFIX.'_map_active', '0');
		add_option( REDE_PREFIX.'_auto_open', '0');
		add_option( REDE_PREFIX.'_cedula', '0');
		add_option( REDE_PREFIX.'_celular', '0');
		add_option( REDE_PREFIX.'_email', '0');
		
	}
}
 