<?php
/**
 * @package Redelocker Boton
 */
namespace Inc\Base;
use \Inc\Base\BaseController;
class Enqueue extends BaseController
{
    /**
     * registers the actions
     */
    public function register()
    {
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue'));
    }
    /**
     * enqueues the scripts and styles
     */
    public function enqueue()
    {
        wp_enqueue_script( 'rededatascript', $this->plugin_url.'assets/rededata.js', [],true);
        wp_enqueue_style( 'rededatastyle', $this->plugin_url.'assets/rededata.css');
    }
}
