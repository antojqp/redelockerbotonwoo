// Create an immediately invoked functional expression to wrap our code
(function () {
    
    // Define our constructor 
    this.redeData = function (usr) {
        // Crea los elementos globales
        this.selectLockers = null;
        this.btnSelect = null;
        this.lockerInfo = null;
        var defaults = {
            usr: "public",
            urlAPI: 'https://www.redelocker.com/funciones/',
            buttonId: 'rede-button',
            divId: 'rededata',
            inputId: 'redeinput',
            autoOpen: false,
            map: false
        }
        this.options = defaults;
        var div = document.getElementById(this.options.divId);
        div.classList.add('cerrado');

        // Create options by extending defaults with the passed in arugments
        if (arguments[0] && typeof arguments[0] === "object") {
            this.options = extendDefaults(defaults, arguments[0]);
        }

        this.triggerButton = document.getElementById(this.options.buttonId);
        if (this.triggerButton) {
            this.triggerButton.addEventListener('click', function (event) {
                event.preventDefault();
                myRedelocker.abrir();
            });
        }

        if (this.options.autoOpen) this.abrir();
    }
    
    // Métodos Públicos
    
    redeData.prototype.cerrar = function () {
        var div = document.getElementById(this.options.divId);
        div.classList.remove("abierto");
        div.classList.add("cerrado");
        if ("createEvent" in document) {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", false, true);
            div.dispatchEvent(evt);
        }
        else div.fireEvent("onchange");
        removeElements.call(this, div);
        
    }

    redeData.prototype.abrir = function () {
        var div = document.getElementById(this.options.divId);
        if (div.classList.contains("cerrado")) {
            div.classList.remove("cerrado");
            div.classList.add("abierto");
            buildOut.call(this);
            initializeEvents.call(this);
            if ("createEvent" in document) {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                div.dispatchEvent(evt);
            }
            else div.fireEvent("onchange");
        }else{
            div.classList.remove("abierto");
            div.classList.add("cerrado");
            removeElements.call(this, div);
            if ("createEvent" in document) {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                div.dispatchEvent(evt);
            }
            else div.fireEvent("onchange");
        }
    }
    
    redeData.prototype.getInfo = function () {
        if(this.selectLockers){
            var id = this.options.inputId;
            var input = document.getElementById(id);
            var lockerText = this.selectLockers[this.selectLockers.selectedIndex].text;
            var lockerId = this.selectLockers.value;
            this.lockerInfo = {
                lockerId: lockerId,
                lockerText: lockerText,
            }
            input.value = '#' + lockerId + '-' + lockerText;
            //trigger the on change event
            if ("createEvent" in document) {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                input.dispatchEvent(evt);
            }
            else input.fireEvent("onchange");
            
            myRedelocker.cerrar();
            return this.lockerInfo;
        }else{
            return null;
        }
    }

    redeData.prototype.removeInfo = function () {
        if(this.selectLockers){
            var id = this.options.inputId;
            var input = document.getElementById(id);
            var lockerText = this.selectLockers[this.selectLockers.selectedIndex].text;
            var lockerId = this.selectLockers.value;
            
            this.lockerInfo = {
                lockerId: lockerId,
                lockerText: lockerText,
            }
            input.value = '';
            var div = document.getElementById(this.options.divId);
            div.classList.remove("abierto");
            div.classList.add("cerrado");
            removeElements.call(this, div);

    
            return this.lockerInfo;
        }else{
            return null;
        }
    }

    // Métodos Privados

    function buildOut() {
        var content;
        ww = window.innerWidth;
        mapa = '';

        if(this.options.map){
            mapa = '<div class="map">'
                    + '<div id="redebotonmap"></div>' 
                 + '</div>';
        }
        
        content = '<div class="top">'
                    +'<div class="logo">'
                        +'<img src="https://redelocker.com/mediakit/redelocker_blanco.png" class="img" alt="Rede Locker">'
                    +'</div>'
                +'</div>'
                +'<div class="options">'
                    +'<div class="form-group">'
                        +'<label for="idUbicacion">Ubicacion de Locker:</label>'
                        +'<select id="redebotonlocker" name="idUbicacion" class="form-control select-css">'
                            +'<option value="-">Seleccione un locker</option>'
                        +'</select>'
                    +'</div>'
                    +mapa
                    +'<div class="select">'
                        +'<a class="btn btn-select right btn-disabled" id="redebotonseleccionar">Selecionar</a>'
                        +'<a class="btn btn-select left btn-disabled" id="redebotoncancelar">Cancelar</a>'
                    +'</div>'
                +'</div>';

        elem = document.getElementById('rededata');
        elem.innerHTML = content;

        this.selectLockers = document.getElementById('redebotonlocker');
        this.btnSelect = document.getElementById('redebotonseleccionar');
        this.btnCancel = document.getElementById('redebotoncancelar');

        var map = this.options.map;
        var url = this.options.urlAPI;
        var divref = this.options.divId;
        getLockers.call(this).then(function (response) {
            var lockersDisp = JSON.parse(response);
            if(map){
                initMap(lockersDisp.lockers, ww, url, divref);
            }
            populateLockers(lockersDisp.lockers);
        });
    }

    function removeElements(div){
        div.innerHTML = '';
    }

    function getMapPoints() {
        var url = this.options.urlAPI;
        var usr = this.options.usr;
        return new Promise(function(resolve, reject){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url +'obtenerLockersBoton.php?usuario=' + usr, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function () {
                if (this.status === 200) {
                    resolve(this.response);
                }
            };
            xhr.send("usuario="+usr);
        });
    }

    function getLockers(){
        var url=this.options.urlAPI;
        var usr = this.options.usr;
        return new Promise(function(resolve, reject){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url + 'obtenerLockersBoton.php?usuario=' + usr, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function () {
                if (this.status === 200) {
                    resolve(this.response);
                }
            };
            xhr.send("usuario=" + usr);
        });
    }

    function populateLockers(options){
        var selectLockers = document.getElementById('redebotonlocker');
        for (i in options) {
            var opt = document.createElement('option');
            opt.appendChild(document.createTextNode(options[i].descripcion));
            opt.value = options[i].idUbicacion;
            selectLockers.appendChild(opt);
        }
    }

    function extendDefaults(source, properties) {
        var property;
        for (property in properties) {
            if (properties.hasOwnProperty(property)) {
                source[property] = properties[property];
            }
        }
        return source;
    }

    function initializeEvents() {
        
        if (this.selectLockers) {
            this.selectLockers.addEventListener('input', setLocker.bind(this));
        }

        if (this.btnSelect) {
            this.btnSelect.addEventListener('click', this.getInfo.bind(this));
        }
        if (this.btnCancel) {
            this.btnCancel.addEventListener('click', this.removeInfo.bind(this));
        }
    }

    var setLocker = function (event) {
        var url = this.options.urlAPI;
        var lockerId = event.target.value;
        checkValues();
    }

    function checkValues() {
        var btnselect = document.getElementById('redebotonseleccionar');
        var btncancel = document.getElementById('redebotoncancelar');
        var locker = document.getElementById('redebotonlocker');
        if(locker.value !== '-'){
            btnselect.classList.add('btn-enabled');
            btnselect.classList.remove('btn-disabled');
            btncancel.classList.add('btn-enabled');
            btncancel.classList.remove('btn-disabled');
        }else{
            btnselect.classList.add('btn-disabled');
            btnselect.classList.remove('btn-enabled');
            btncancel.classList.add('btn-enabled');
            btncancel.classList.remove('btn-disabled');
        }
    }

    // Inicializa el mapa
    function initMap(marcadores, ww, urlref, divref) {
        var map = new google.maps.Map(document.getElementById('redebotonmap'), {
            center: {
                lat: -34.807934,
                lng: -56.205398
            },
            zoom: 10,
            mapTypeControl: false,
            fullscreenControl: false,
            streetViewControl: false,
            styles: [{
                featureType: 'all',
                elementType: 'all'
            }]
        });

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (id in marcadores) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(marcadores[id].latitud, marcadores[id].longitud),
                label: {
                    text: marcadores[id].idUbicacion,
                    color: "#ffffff",
                    fontSize: "0px",
                    fontWeight: "bold"
                },
                map: map
            });

            // Listener MouseOver
            if (ww > 415) {
                google.maps.event.addListener(marker, 'mouseover', (function (marker, id) {
                    return function () {
                        var info = marcadores[id];
                        infowindow.setContent(
                                "<b>#"+info.idUbicacion+" "+info.descripcion+"</b><br/>"+
                                info.calle+" "+info.numero+"<br/>"+
                                (info.esquina1 != "" ? ("Esq. "+info.esquina1+"<br/>") : "")+
                                (info.horario != "" ? info.horario : "")
                            );
                        infowindow.open(map, marker);
                    }
                })(marker, id));

                // Listener MouseOut
                google.maps.event.addListener(marker, 'mouseout', (function (marker, id) {
                    return function () {
                        infowindow.close(map, marker);
                    }
                })(marker, id));
            }

            // Listener on Click
            google.maps.event.addListener(marker, 'click', (function (marker, id) {
                return function () {
                    var lock_info = document.querySelector(".locker-information");
                    var lockers = document.getElementById('redebotonlocker');
                    for (var i = 0; i < lockers.length; i++) {
                        if (lockers.options[i].value == marker.label.text) {
                            lockers.selectedIndex = i;
                            checkValues();
                        }
                    }
                }
            })(marker, id));
        }
    }

}());